# Exemplo de configuração do NFS com swarm

Esse repositório é apenas um exemplo de como configurar os volumes de um serviço
para serem montados via rede, em um servidor NFS. O servidor pode estar rodando
tanto local, quanto remoto. Para o caso de um servidor remoto, use o seu ip
externo, na variável `addr`, na definição do volume. A variável `vers` define
qual a versão do cliente nfs será utilizada para interagir com o servidor. Essa
versão deve ser suportada pelo servidor. Para mais informações acesse esse
[link](https://docs.oracle.com/cd/E19253-01/816-4555/rfsadmin-224/index.html).
As imagens definidas no arquivo de stack são ficticias, adapte o arquivo às suas
necessidades.

Você precisará configurar o servidor NFS usando o arquivo `exports`
como base, esse README não cobre essa configuração.
Os diretórios definidos na diretiva `device`, tem que existir no servidor nfs
antes do deploy do arquivo de stack. Esses diretórios também tem que estar
exportados no arquivo `exports`, do contrário o cliente do nfs não terá permissão
de montar os volumes via rede.

Qualquer diretório existente dentro do container pode ser montado via nfs,
desde que as devidas configurações nos volumes e no servidor NFS sejam feitas.
Tome cuidado caso exista um firewall interceptando as requisições realizadas
pelo cliente nfs no servidor, isso pode travar o mount e deixar o serviço em
`pending` por um longo tempo. Antes de rodar o deploy, execute um `mount` usando
um dos diretórios definidos no arquivo `exports`, apontando para o servidor NFS.
Caso o mount manual funcione, o deploy da stack também irá funcionar corretamente.
Para informações sobre como montar um diretório usando o comando `mount` no NFS, acesse esse [link](https://docs.oracle.com/cd/E19683-01/817-6960/fsmount-69423/index.html).

